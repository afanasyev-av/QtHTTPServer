#include <QCoreApplication>

#include "qsimplehttpserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QSimpleHTTPServer server;
    server.StartServer();
    return a.exec();
}
