#include "qsimplehttpserver.h"

QSimpleHTTPServer::QSimpleHTTPServer(QObject *parent):
    QTcpServer(parent)
{
    threadpool = new QThreadPool(this);
}

void QSimpleHTTPServer::incomingConnection(qintptr handle)
{
    QSocketRunnable *runnable = new QSocketRunnable(handle);
    threadpool->start(runnable);
}

void QSimpleHTTPServer::StartServer(void)
{
    if(listen(QHostAddress::Any,8080)){
        qDebug() << "Listen good" << endl;
    }
    else{
        qDebug() << "error Listening" << errorString() << endl;
    }
}



