#include "qsocketrunnable.h"

QSocketRunnable::QSocketRunnable(qintptr handle):
    descriptor(handle)
{
    codeHTTP[100] = "Continue";
    codeHTTP[101] = "Switching Protocols";
    codeHTTP[102] = "Processing";
    codeHTTP[200] = "OK";
    codeHTTP[201] = "Created";
    codeHTTP[202] = "Accepted";
    codeHTTP[203] = "Non-Authoritative Information";
    codeHTTP[204] = "No Content";
    codeHTTP[205] = "Reset Content";
    codeHTTP[206] = "Partial Content";
    codeHTTP[207] = "Multi-Status";
    codeHTTP[226] = "IM Used";
    codeHTTP[300] = "Multiple Choices";
    codeHTTP[301] = "Moved Permanently";
    codeHTTP[302] = "Moved Temporarily";
    codeHTTP[302] = "Found";
    codeHTTP[303] = "See Other";
    codeHTTP[304] = "Not Modified";
    codeHTTP[305] = "Use Proxy";
    codeHTTP[306] = "—";
    codeHTTP[307] = "Temporary Redirect";
    codeHTTP[400] = "Bad Request";
    codeHTTP[401] = "Unauthorized";
    codeHTTP[402] = "Payment Required";
    codeHTTP[403] = "Forbidden";
    codeHTTP[404] = "Not Found";
    codeHTTP[405] = "Method Not Allowed";
    codeHTTP[406] = "Not Acceptable";
    codeHTTP[407] = "Proxy Authentication Required";
    codeHTTP[408] = "Request Timeout";
    codeHTTP[409] = "Conflict";
    codeHTTP[410] = "Gone";
    codeHTTP[411] = "Length Required";
    codeHTTP[412] = "Precondition Failed";
    codeHTTP[413] = "Request Entity Too Large";
    codeHTTP[414] = "Request-URI Too Large";
    codeHTTP[415] = "Unsupported Media Type";
    codeHTTP[416] = "Requested Range Not Satisfiable";
    codeHTTP[417] = "Expectation Failed";
    codeHTTP[422] = "Unprocessable Entity";
    codeHTTP[423] = "Locked";
    codeHTTP[424] = "Failed Dependency";
    codeHTTP[425] = "Unordered Collection";
    codeHTTP[426] = "Upgrade Required";
    codeHTTP[428] = "Precondition Required";
    codeHTTP[429] = "Too Many Requests";
    codeHTTP[431] = "Request Header Fields Too Large";
    codeHTTP[444] = "-";
    codeHTTP[449] = "Retry With";
    codeHTTP[451] = "Unavailable For Legal Reasons";
    codeHTTP[500] = "Internal Server Error";
    codeHTTP[501] = "Not Implemented";
    codeHTTP[502] = "Bad Gateway";
    codeHTTP[503] = "Service Unavailable";
    codeHTTP[504] = "Gateway Timeout";
    codeHTTP[505] = "HTTP Version Not Supported";
    codeHTTP[506] = "Variant Also Negotiates";
    codeHTTP[507] = "Insufficient Storage";
    codeHTTP[508] = "Loop Detected";
    codeHTTP[509] = "Bandwidth Limit Exceeded";
    codeHTTP[510] = "Not Extended";
    codeHTTP[511] = "Network Authentication Required";
    codeHTTP[520] = "Web server is returning an unknown error";
    codeHTTP[521] = "Web server is down";
    codeHTTP[522] = "Connection timed out";
    codeHTTP[523] = "Origin is unreachable";
    codeHTTP[524] = "A timeout occurred";
    codeHTTP[525] = "SSL handshake failed";
    codeHTTP[526] = "Invalid SSL certificate";
}

void QSocketRunnable::run()
{
    QTcpSocket *socket = new QTcpSocket();
    socket->setSocketDescriptor(descriptor);

    socket->waitForReadyRead();
    QByteArray responce = decode(QString(socket->readAll()));

    socket->write(responce);
    socket->waitForBytesWritten();

    socket->disconnectFromHost();

    socket->close();
    socket->deleteLater();
}
QByteArray QSocketRunnable::decode(QString str)
{
    QMap<QString,QString> header;
    //qDebug() << str << endl;
    QStringList strlst = str.split(QRegExp("\\r\\n\\r\\n"));
    //for(int i=0; i<strlst.size(); i++){
    //    qDebug() << i << ":" << strlst[i] << endl;
    //}
    QString head = strlst[0];
    QString body = strlst[0];

    strlst.clear();
    QString responce = "HTTP/1.1 %1 %2\r\n\r\n%3";
    strlst = head.split(QRegExp("\\r\\n"));
    QStringList strlsthead;
    for(int i=1; i<strlst.size(); i++){
        QString key,value;
        strlsthead.clear();
        strlsthead = strlst[i].split(": ");
        key = strlsthead[0];
        value = strlsthead[1];
        //qDebug() << i << ":" << strlst[i].split(": ") << endl;
        header.insert(key,value);
    }
    strlsthead.clear();
    strlsthead = strlst[0].split(" ");

    QString method = "";
    if(strlsthead.size()>0){
        method = strlsthead[0];
    }
    QString fileName = "";
    if(strlsthead.size()>1){
        fileName = strlsthead[1];
    }
    if(strlsthead.size()!=3){
        QString html = "<html>\
                    <head><title>title</title></head>\
                    <body bgcolor='white'>\
                    <center><h1>400 Bad Request</h1></center>\
                    <hr><center>myserver</center>\
                    </body>\
                    </html>";
        return responce.arg(400).arg(codeHTTP[400]).arg(html).toUtf8();
    }

    qDebug() << "method: " << method << endl;
    qDebug() << "file: " << fileName << endl;

    if(method == "GET"){
        QFile file("."+fileName);
        QMimeDatabase mimeDatabase;
        QMimeType mimeType;
        mimeType = mimeDatabase.mimeTypeForFile(QFileInfo(file));
        QByteArray data;
        if (!file.open(QIODevice::ReadOnly)){
            QString html = "<html>\
                    <head><title>title</title></head>\
                    <body bgcolor='white'>\
                    <center><h1>404 Not Found</h1></center>\
                    <hr><center>myserver</center>\
                    </body>\
                    </html>";
            return responce.arg(404).arg(codeHTTP[404]).arg(html).toUtf8();
        }
        data = file.readAll();
        if(mimeType.name().left(5) == "image"){
            //qDebug() << data.toBase64().size() << endl;
            QString responce = "HTTP/1.1 %1 %2\r\nContent-Type: image/jpeg\r\nContent-Length: %3\r\n\r\n";
            return responce.arg(200).arg(codeHTTP[200]).arg(file.size()).toUtf8().append(data);
        }
        else{
            return responce.arg(200).arg(codeHTTP[200]).arg("").toUtf8().append(data);;
        }
    }
    else{
        QString html = "<html>\
                <head><title>title</title></head>\
                <body bgcolor='white'>\
                <center><h1>501 Not Implemented</h1></center>\
                <hr><center>myserver</center>\
                </body>\
                </html>";
        return responce.arg(501).arg(codeHTTP[501]).arg(html).toUtf8();
    }
}

