#ifndef QSIMPLEHTTPSERVER_H
#define QSIMPLEHTTPSERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include <QDateTime>

#include <QThreadPool>
#include "qsocketrunnable.h"

class QSimpleHTTPServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit QSimpleHTTPServer(QObject *parent = 0);
    void incomingConnection(qintptr handle);
    void StartServer(void);

private:
    QThreadPool *threadpool;
};

#endif // QSIMPLEHTTPSERVER_H
