#ifndef QSOCKETRUNNABLE_H
#define QSOCKETRUNNABLE_H

#include <QRunnable>
#include <QTcpSocket>
#include <QDebug>
#include <QDateTime>
#include <QFile>
#include <QMap>
#include <QMimeDatabase>
#include <QMimeType>
#include <QFileInfo>

class QSocketRunnable : public QRunnable
{
public:
    QSocketRunnable(qintptr handle);

    void run();
private:
    qintptr descriptor;
    QByteArray decode(QString str);
    QMap<int,QString> codeHTTP;
};

#endif // QSOCKETRUNNABLE_H
